//Jeanine Burke (jvburke@wustl.edu)
//Lab1
//ReadMe.txt


	The purpose of lab1 is to use a struct to represent game pieces and create a game board
by using file streams, string streams, and indexing a vector whose elements are a struct.
	When implementing my design for the program, I made sure to learn from the mistakes that 
I made while writing lab0. Therefore, I placed function calls after their definition and made
sure to include all header files in the source files. With this design, I had a much easier
time compiling my program than I did in lab0.
	The major challenge of this lab was learning how to implement struct. A major error that
I ran into involved the definition and declaration of the struct game_piece. Similar to a 
function, I initially declared the struct in gamePiece.h and then defined it in gamePiece.cpp
However, when initializing a new instance of struct in a separate source file, I ran
into several errors, all saying "Error: Incomplete type is not allowed." This error makes 
sense because at the top of my source files, I included "gamePiece.h," which only held an 
empty declaration of the struct. Therefore, I ran into errors when trying to initialize
components of game_piece. I fixed this error by writing a full definition game_piece in
gamePiece.h, therefore all other source files that create a new game_piece can have access to 
the complete struct. After this error was fixed, the program could run correctly.
 

Cases:
Command: H:\My Documents\Visual Studio 2012\Projects\lab1\Debug>lab1 chess.txt
chess.txt contains:
8 8
white pawn P 0 1
white pawn P 1 1
white pawn P 2 1
white pawn P 3 1
white pawn P 4 1
white pawn P 5 1
white pawn P 6 1
white pawn P 7 1
white rook P 0 0
white knight S 1 0
white bishop B 2 0
white queen Q 3 0
white king K 4 0
white bishop B 5 0
white knight S 6 0
white rook R 7 0
black rook r 0 7
black knight s 1 7
black bishop b 2 7
black queen q 3 7
black king k 4 7
black bishop b 5 7
black knight s 6 7
black rook r 7 7
black pawn p 0 6
black pawn p 1 6
black pawn p 2 6
black pawn p 3 6
black pawn p 4 6
black pawn p 5 6
black pawn p 6 6 
black pawn p 7 6

Output:

rsbqkbsr
pppppppp                                




PPPPPPPPP
SBQKBSR


This output is correct upon initial consideration because the board is shown to have 8x8 dimensions,
which was given on the first line of the input. As shown, the board correctly preserves the location 
of empty spaces while placing game pieces. The program was told to print the game board with its
vertical dimension(y) in descending order and the horizontal dimension(x) in ascending order. Therefore, 
this output is correct because it starts at the upper left square of the board(the greatest horizontal
dimension and the least vertical dimension). The program then proceeds to print out the entire row 
(this is increasing the x-coordinate value while the y-coordinate remains the same) until it finishes
and moves on to the next row under, again printing from left to right. After the board it printed, it
is clear that this is the layout of a standard chessboard before gaming commences.


Command: H:\My Documents\Visual Studio 2012\Projects\lab1\Debug>lab1 checkers.txt
checkers.txt contains:

8 8
black checker X 0 0
black checker X 0 2
black checker X 1 1
black checker X 2 0
black checker X 2 2
black checker X 3 1
black checker X 4 0
black checker X 4 2
black checker X 5 1
black checker X 6 0
black checker X 6 2
black checker X 7 1
red checker O 0 6
red checker O 1 5
red checker O 1 7
red checker O 2 6
red checker O 3 5
red checker O 3 7
red checker O 4 6
red checker O 5 5
red checker O 5 7
red checker O 6 6
red checker O 7 5
red checker O 7 7

Output:

 O O O O
O O O O 
 O O O O

X X X X
 X X X X
X X X X


This output is also correct. The program has created a board of the proper dimensions and has
correctly preserved the empty squares. As described above, the board correctly prints the board
with vertical dimensions in descending order and horizontal dimensions in ascneding order. This
output correctly displays a standard checkers board before gaming commences.

extra credit


	In order to find all adjacencies of a gamepiece, I needed to consider all neighboring squares. There
are 8 max possible neighboring squares. For each occupied square on the board, I needed to check for
the existence of neighboring squares. Say the game piece in question is station at coordinate (x,y).
To check for the existence of neighboring squares, I looked to see which of the following eight
coordinates were within the dimensions of the game board: (x+1,y+1), (x,y+1), (x-1,y+1), (x-1,y),
(x-1,y-1), (x,y-1), (x+1,y-1), and (x+1,y). After all neighboring squares were established, I then
needed to check which neighbors were occupied. These adjacent game pieces were then printed out after 
the game piece under consideration.
	With the extra credit implemented, lab1 will print out the board, as well as all sets of 
adjacencies. Given below is the set of all adjacent game pieces, which is a fragment of the final 
output. With the same command line as above, the program will output both the board and the sets of adjacent
game pieces. 


Output (checkers.txt):

1,7 red checker: 0,6 red checker; 2,6 red checker;

3,7 red checker: 2,6 red checker; 4,6 red checker;

5,7 red checker: 4,6 red checker; 6,6 red checker;

7,7 red checker: 6,6 red checker;

0,6 red checker: 1,7 red checker; 1,5 red checker;

2,6 red checker: 1,7 red checker; 3,7 red checker; 1,5 red checker; 3,5 red chec
ker;

4,6 red checker: 3,7 red checker; 5,7 red checker; 3,5 red checker; 5,5 red chec
ker;

6,6 red checker: 5,7 red checker; 7,7 red checker; 5,5 red checker; 7,5 red chec
ker;

1,5 red checker: 0,6 red checker; 2,6 red checker;

3,5 red checker: 2,6 red checker; 4,6 red checker;

5,5 red checker: 4,6 red checker; 6,6 red checker;

7,5 red checker: 6,6 red checker;

0,2 black checker: 1,1 black checker;

2,2 black checker: 1,1 black checker; 3,1 black checker;

4,2 black checker: 3,1 black checker; 5,1 black checker;

6,2 black checker: 5,1 black checker; 7,1 black checker;

1,1 black checker: 0,2 black checker; 2,2 black checker; 0,0 black checker; 2,0
black checker;

3,1 black checker: 2,2 black checker; 4,2 black checker; 2,0 black checker; 4,0
black checker;

5,1 black checker: 4,2 black checker; 6,2 black checker; 4,0 black checker; 6,0
black checker;

7,1 black checker: 6,2 black checker; 6,0 black checker;

0,0 black checker: 1,1 black checker;

2,0 black checker: 1,1 black checker; 3,1 black checker;

4,0 black checker: 3,1 black checker; 5,1 black checker;

6,0 black checker: 5,1 black checker; 7,1 black checker;


This output is correct. Only occupied squares are printed. Game pieces that are located in corners 
and along edges are correctly outputted with their neighbors. For example, the output shows that
a black checker piece located at (0,0) on the board will have only one neighbor at (1,1), another
black checker. This makes sense when considering the configuration of a standard checker board.



Output (chess.txt):



0,7 black rook: 1,7 black knight; 0,6 black pawn; 1,6 black pawn;

1,7 black knight: 2,7 black bishop; 1,6 black pawn; 0,6 black pawn; 2,6 black pa
wn; 0,7 black rook;

2,7 black bishop: 3,7 black queen; 2,6 black pawn; 1,6 black pawn; 3,6 black paw
n; 1,7 black knight;

3,7 black queen: 4,7 black king; 3,6 black pawn; 2,6 black pawn; 4,6 black pawn;
 2,7 black bishop;

4,7 black king: 5,7 black bishop; 4,6 black pawn; 3,6 black pawn; 5,6 black pawn
; 3,7 black queen;

5,7 black bishop: 6,7 black knight; 5,6 black pawn; 4,6 black pawn; 6,6 black pa
wn; 4,7 black king;

6,7 black knight: 7,7 black rook; 6,6 black pawn; 5,6 black pawn; 7,6 black pawn
; 5,7 black bishop;

7,7 black rook: 7,6 black pawn; 6,6 black pawn; 6,7 black knight;

0,6 black pawn: 1,6 black pawn; 0,7 black rook; 1,7 black knight;

1,6 black pawn: 2,6 black pawn; 1,7 black knight; 0,7 black rook; 2,7 black bish
op; 0,6 black pawn;

2,6 black pawn: 3,6 black pawn; 2,7 black bishop; 1,7 black knight; 3,7 black qu
een; 1,6 black pawn;

3,6 black pawn: 4,6 black pawn; 3,7 black queen; 2,7 black bishop; 4,7 black kin
g; 2,6 black pawn;

4,6 black pawn: 5,6 black pawn; 4,7 black king; 3,7 black queen; 5,7 black bisho
p; 3,6 black pawn;

5,6 black pawn: 6,6 black pawn; 5,7 black bishop; 4,7 black king; 6,7 black knig
ht; 4,6 black pawn;

6,6 black pawn: 7,6 black pawn; 6,7 black knight; 5,7 black bishop; 7,7 black ro
ok; 5,6 black pawn;

7,6 black pawn: 7,7 black rook; 6,7 black knight; 6,6 black pawn;

0,1 white pawn: 1,1 white pawn; 0,0 white rook; 1,0 white knight;

1,1 white pawn: 2,1 white pawn; 1,0 white knight; 0,0 white rook; 2,0 white bish
op; 0,1 white pawn;

2,1 white pawn: 3,1 white pawn; 2,0 white bishop; 1,0 white knight; 3,0 white qu
een; 1,1 white pawn;

3,1 white pawn: 4,1 white pawn; 3,0 white queen; 2,0 white bishop; 4,0 white kin
g; 2,1 white pawn;

4,1 white pawn: 5,1 white pawn; 4,0 white king; 3,0 white queen; 5,0 white bisho
p; 3,1 white pawn;

5,1 white pawn: 6,1 white pawn; 5,0 white bishop; 4,0 white king; 6,0 white knig
ht; 4,1 white pawn;

6,1 white pawn: 7,1 white pawn; 6,0 white knight; 5,0 white bishop; 7,0 white ro
ok; 5,1 white pawn;

7,1 white pawn: 7,0 white rook; 6,0 white knight; 6,1 white pawn;

0,0 white rook: 1,0 white knight; 0,1 white pawn; 1,1 white pawn;

1,0 white knight: 2,0 white bishop; 1,1 white pawn; 0,1 white pawn; 2,1 white pa
wn; 0,0 white rook;

2,0 white bishop: 3,0 white queen; 2,1 white pawn; 1,1 white pawn; 3,1 white paw
n; 1,0 white knight;

3,0 white queen: 4,0 white king; 3,1 white pawn; 2,1 white pawn; 4,1 white pawn;
 2,0 white bishop;

4,0 white king: 5,0 white bishop; 4,1 white pawn; 3,1 white pawn; 5,1 white pawn
; 3,0 white queen;

5,0 white bishop: 6,0 white knight; 5,1 white pawn; 4,1 white pawn; 6,1 white pa
wn; 4,0 white king;

6,0 white knight: 7,0 white rook; 6,1 white pawn; 5,1 white pawn; 7,1 white pawn
; 5,0 white bishop;

7,0 white rook: 7,1 white pawn; 6,1 white pawn; 6,0 white knight;

This output is also correct. Only occupied squares are printed. Game pieces that are located in corners 
and along edges are correctly outputted with their neighbors. Taking a look again at the game piece 
located at (0,0), we see that it has three adjacencies, all white game pieces. Knowing the standard
configuration of a chess board, this output is correct. Unlike a checker board, a chessboard will show
more neighbors per game piece due to it configuration. 