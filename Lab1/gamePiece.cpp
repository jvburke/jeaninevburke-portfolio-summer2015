//Author: Jeanine Burke (jvburke@wustl.edu)
//Lab1
//gamePiece.cpp 

#include "stdafx.h"
#include "gamePiece.h"
#include "gameBoard.h"
#include "lab1.h"
#include <string>
#include <iostream>
using namespace std;


string eString(piece_color &piece_color){
	
	//converts the value of enum type to a string
	switch(piece_color){
	case RED: 
		return string("red"); 
		break;
	case BLACK: 
		return string("black");
		break;
	case WHITE: 
		return string("white");
		break;
	case NO_COLOR: 
		return string("no color");
		break;
	default:{
		return string("invalid color");
		break;
			}
	}

}


enum piece_color enumInt(string &string){

	//converts string to enum value 
	if(string == "red"){
		return RED;
	}
	else if(string == "black"){
		return BLACK;
	}
	else if(string == "white"){
		return WHITE;
	}
	else if(string == " "){
		return NO_COLOR;
	}
	else{
		return INVALID_COLOR;
	}
}

