//Author: Jeanine Burke (jvburke@wustl.edu)
//Lab1
//gameBoard.cpp

#include "stdafx.h"
#include "gameBoard.h"
#include "gamePiece.h"
#include "lab1.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;
using std::ifstream;


int readBoard(ifstream &ifs, int &int1, int &int2){

	string line;

	//if the file is able to be opened, continue
	if(ifs.is_open()) {

		//we only need to read the first line of a file
		if(getline(ifs, line)){

			//discard blank lines
			if(!line.empty()){

				//if the dimensions are sucessfully read
				if(stringstream(line) >> int1 >>int2){
					return SUCCESS;
				}
				else{
					return ERROR_INCORRECT_DIMENSIONS; 
				}
			}
		}
		else{
			return ERROR_READING_ALL_FILE_LINES;
		}

		ifs.close();
	}

	return ERROR_OPENING_FILE;
}


int readPiece(ifstream &ifs, vector<game_piece> &vectorPiece,int horzDim, int vertDim){

	string line;
	while(getline(ifs, line)){
		//discard blank lines
		if(!line.empty()){

			string color;
			string pieceName1;
			string display1;
			int horizontal;
			int vertical;
			

			if(stringstream(line) >> color>>pieceName1>>display1>>horizontal>>vertical){

				//convert the string color to enum value
				enum piece_color enumint = enumInt(color);

				//check if game piece is invalid or its location goes beyond board dimensions
				if( enumint ==INVALID_COLOR || horizontal > horzDim || vertical > vertDim){
					//skip to next line
					stringstream(line).ignore();
				}
				else{
					//insert valid game piece into vector
					int index = horzDim*vertical+horizontal;
					game_piece newPiece;

					newPiece.piece_color = enumint;
					newPiece.pieceName = pieceName1;
					newPiece.display = display1;

					vectorPiece[index] = newPiece;

				}

			}
			else{
				return ERROR_OPENING_FILE;
			}

		}
	}

	return SUCCESS;
}


int printBoard(const vector<game_piece> &vectorPiece, int int1, int int2){

	//unsigned size is the size of vectorPiece (ie. number of squares on board)
	unsigned size=int1*int2;

	//check if the vector size is compatible with game board size
	if(vectorPiece.size() > size || vectorPiece.size() < size){
		return ERROR_VECTOR_SIZE;
	}

	//vertical dimensions in descending order
	//horizontal dimensions in ascending order
	for(int k=int2-1; k>=0; k--){
			
		for(int i=0; i<int1; i++){
		
			cout<<vectorPiece[int1*k+i].display;
			//at the end of a row, go to next next row under
			if(i==int1-1){
				cout<<"\n"<<flush;
			}

		}
	}


	return SUCCESS;
}


//EXTRACREDIT

int printAdj(const vector<game_piece> &vectorPiece, int int1, int int2){

	cout<<"\n"<<endl;

	for(int k=int2-1; k>=0; k--){
		for(int i=0; i<int1; i++){



			piece_color c = vectorPiece[int1*k+i].piece_color;
			piece_color col;
			//do not consider empty spaces
			if(c!=NO_COLOR){

				cout<< i << "," << k << " " << eString(c) << " " << vectorPiece[int1*k+i].pieceName<< ": "<<flush;
				//check all 8 neighboring squares
				if((i+1) >= 0 && (i+1) < int1){
					if(vectorPiece[int1*k+(i+1)].piece_color!=NO_COLOR){
						col = vectorPiece[int1*k+(i+1)].piece_color;
						cout<< i+1 << "," << k << " " << eString(col) << " " << vectorPiece[int1*k+(i+1)].pieceName<< "; "<<flush;
					}

				}

				if((k+1) >=0 && (k+1) <int2){

					if(vectorPiece[int1*(k+1)+i].piece_color!=NO_COLOR){

						col = vectorPiece[int1*(k+1)+i].piece_color;
						cout<< i << "," << k+1 << " " << eString(col) << " " << vectorPiece[int1*(k+1)+i].pieceName<< "; "<<flush;
					}

					if((i-1)>=0 && (i-1) < int1){

						if(vectorPiece[int1*(k+1)+(i-1)].piece_color!=NO_COLOR){
							col = vectorPiece[int1*(k+1)+(i-1)].piece_color;
							cout<< i-1 << "," << k+1 << " " << eString(col) << " " << vectorPiece[int1*(k+1)+(i-1)].pieceName<< "; "<<flush;
						}

					}

					if((i+1) <int1){

						if(vectorPiece[int1*(k+1)+(i+1)].piece_color!=NO_COLOR){
							col = vectorPiece[int1*(k+1)+(i+1)].piece_color;
							cout<< i+1 << "," << k+1 << " " << eString(col) << " " << vectorPiece[int1*(k+1)+(i+1)].pieceName<< "; "<<flush;
						}
					}

				}



				if((k-1) >=0 && (k-1) < int2){

					if(vectorPiece[int1*(k-1)+i].piece_color!=NO_COLOR){
						col = vectorPiece[int1*(k-1)+i].piece_color;
						cout<< i << "," << k-1 << " " << eString(col) << " " << vectorPiece[int1*(k-1)+i].pieceName<< "; "<<flush;
					}

					if((i-1)>=0 && (i-1) < int1){

						if(vectorPiece[int1*(k-1)+(i-1)].piece_color!=NO_COLOR){
							col = vectorPiece[int1*(k-1)+(i-1)].piece_color;
							cout<< i-1 << "," << k-1 << " " << eString(col) << " " << vectorPiece[int1*(k-1)+(i-1)].pieceName<< "; "<<flush;
						}

					}

					if((i+1) <int1){

						if(vectorPiece[int1*(k-1)+(i+1)].piece_color!=NO_COLOR){
							col = vectorPiece[int1*(k-1)+(i+1)].piece_color;
							cout<< i+1 << "," << k-1 << " " << eString(col) << " " << vectorPiece[int1*(k-1)+(i+1)].pieceName<< "; "<<flush;
						}
					}
				}



				if((i-1) >=0){

					if(vectorPiece[int1*k+(i-1)].piece_color!=NO_COLOR){
						col = vectorPiece[int1*k+(i-1)].piece_color;
						cout<< i-1 << "," << k << " " << eString(col) << " " << vectorPiece[int1*k+(i-1)].pieceName<< "; "<<flush;
					}

				}

				cout<<"\n"<<endl;

			}

		}
	}

	return SUCCESS;

}

