//lab1.h
//Jeanine Burke (jvburke@wustl.edu)
//lab0.h
	

#ifndef LAB1_H
#define LAB1_H

#include<string>
using std::string;



enum readArgument{
	PROGRAM_NAME =0,	//0
	INPUT_FILE = 1,		//1
	EXPECTED_ARGUMENTS = 2,		//2
};

enum Result{
	SUCCESS = 0,
	ERROR_OPENING_FILE = 1,
	ERROR_TOO_FEW_ARGUMENTS = 2,
	ERROR_INCORRECT_DIMENSIONS = 3,
	ERROR_READING_ALL_FILE_LINES = 4,
	ERROR_VECTOR_SIZE = 5

};


int usage(string progName, string message);

int toLowercase(string &str);

#endif