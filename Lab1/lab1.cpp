//Author: Jeanine Burke (jvburke@wustl.edu)
//Lab1
//lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "lab1.h"
#include "gamePiece.h"
#include "gameBoard.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;
using std::vector;
using std::ifstream;
using std::string;


int usage(string progName, string message){

	cout<< "usage: " << progName << " " << message << endl;

	return ERROR_TOO_FEW_ARGUMENTS;

}

int toLowercase(string &str){

	for(size_t i=0; i<str.length(); ++i){
		//via ASCII table
		//str[i]>=A&&str[i]<=Z
		if(str[i]>=65&&str[i]<=90){
			//convert to lowercase
			str[i]=str[i]+32;

		}

	}
	return SUCCESS;

}


int main(int argc, char* argv[])
{

	if(argc != EXPECTED_ARGUMENTS){
		usage(argv[PROGRAM_NAME], "<input_file.txt>");
	}
	else{

		ifstream ifs(argv[INPUT_FILE]);

		string line;

		if(ifs.is_open()){

			int horzExtent;
			int vertExtent;

			int res = readBoard(ifs, horzExtent, vertExtent);

			while(res !=SUCCESS){

				if(res==ERROR_INCORRECT_DIMENSIONS){
					return ERROR_INCORRECT_DIMENSIONS;
					break;
				}
				else if(res==ERROR_READING_ALL_FILE_LINES){
					return ERROR_READING_ALL_FILE_LINES;
					break;
				}
				else{
					res = readBoard(ifs, horzExtent, vertExtent);
				}
			}
			vector<game_piece> v;

			//create empty game pieces for the board
			for(int i=0; i<(horzExtent*vertExtent); i++){
				//create an empty game piece
				game_piece emptyPiece;				 
				emptyPiece.piece_color=NO_COLOR;
				emptyPiece.pieceName=" ";
				emptyPiece.display=" ";

				v.push_back(emptyPiece);

			}

			int result=readPiece(ifs, v, horzExtent, vertExtent);

			if(result!=SUCCESS){
				return ERROR_OPENING_FILE;
			}
			else{

				int pB = printBoard(v, horzExtent, vertExtent);

				//EXTRA CREDIT
				int aB = printAdj(v, horzExtent, vertExtent);

				return pB;
			}
		}


		else{
			return ERROR_OPENING_FILE;
		}

	}

}

