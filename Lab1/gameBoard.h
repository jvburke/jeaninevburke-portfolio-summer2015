//gamePiece.h
//Jeanine Burke (jvburke@wustl.edu)
//gamePiece.h 

#ifndef READ_BOARD
#define READ_BOARD


#include "gamePiece.h"
#include <vector>
#include <fstream>
#include <fstream>
using std::ifstream;
using std::ifstream;
using std::vector;

int readBoard(ifstream &ifs, int &int1, int &int2);


int readPiece(ifstream &ifs, vector<game_piece> &vectorPiece,int horzDim, int vertDim);


int printBoard(const vector<game_piece> &vectorPiece, int int1, int int2);


int printAdj(const vector<game_piece> &vectorPiece, int int1, int int2);


#endif