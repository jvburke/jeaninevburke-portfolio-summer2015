//gamePiece.h
//Jeanine Burke (jvburke@wustl.edu)
//Lab1
//gamePiece.h

#ifndef GAMEPIECE_H
#define GAMEPIECE_H

#include <string>
using std::string;

enum piece_color{
	RED =0,			
	BLACK = 1,		
	WHITE = 2,
	INVALID_COLOR = 3,
	NO_COLOR = 4
};


struct game_piece{

	piece_color piece_color;
	string pieceName;
	string display;	
	
};


enum piece_color enumInt(string &string);


string eString(piece_color &piece_color);
#endif
