<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title> Admin Page</title>
        
        <style type="text/css">

        	html { 
  				background: url(https://dl.dropboxusercontent.com/u/159328383/background.jpg) no-repeat center center fixed; 
  				background-size: cover;
			}
        	body{
        		text-align: center;
        		font: 12px/16px Veranda, sans-serif;
				background-color: rgba(255, 255, 255, 0.8);
				width: 60%; /*test black element (center)*/
				margin-right: auto;
				margin-left: auto;
				padding: 0;
				border: 14px solid #000000;
        	
        	}
        	
        
        
     	</style>   
    </head>
    <body>
    	<br><br><br>
        <?php
	        session_start();
	        
	        $_SESSION['is_admin'] = true;
	        
            $dir = "/home/rockyli/user_info/users_folder";
            $adminfile = scandir($dir);
            $_SESSION['adminFile'] = $adminfile;
            
            echo "<h2>All Users Are Listed Here: </h2>";
            foreach($adminfile as $file1) {
                if($file1 != "." && $file1 != "..") {
                    echo "<p><li><h3>$file1</h3></li></p>";
                }
            }                
        ?>
        <div class="select">
			<form action="index.php" method="post" enctype="multipart/form-data">
				<label>Choose the user's folder you want to access: &nbsp&nbsp&nbsp</label>
				<select name="accessfile">
					<?php
						foreach($_SESSION['adminFile'] as $file2) {
							if($file2 != "." && $file2 != "..") {
								echo "<option value=\"$file2\">$file2</option>";	
							}
						}
					?>
				</select>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<input type="submit" value="Access"/> 
				<br><br><br>
			</form>	
		</div>

    </body>
</html>