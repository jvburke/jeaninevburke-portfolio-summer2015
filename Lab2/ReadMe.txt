//Jeanine Burke (jvburke@wustl.edu)
//lab2
//ReadMe.txt

	The purpose of the lab is to develop a game that extends knowledge of C++ language features, including
functions, classes, the IO Library, and sequential containers. The program should encapsulate game data
within a class, prompt the user for input game moves, determine whether or not a game is valid, and update
the game board when a valid move is entered by the user.

	The instructions were intentionally vague on the details of the class NineAlmondsGame. Therefore,
when it came to deciding which aspects of the class to make private, there was some leeway. The instructions
stated that the name and color of the game pieces must be private. The layout of these game pieces on the board
must also be private. Similar to lab1, I made a struct of game pieces and creates a vector of game pieces. This 
vector was then made private. Therefore, the information on the game pieces and the game board remains private.

	For efficiency's sake, I created another method for the class NineAlmondsGame that decides whether or not
a proposed move is valid. This method is used in both the turn() and stalemate() methods.

	 An error that I encountered during this lab involved the ostream operator of the friends class.After declaring 
the method in the class header, I was still unable to access the vector of game pieces. However, this error was due
to not including the header "using std::ostream" in the NineAlmondsGame.h header. After this statement was included,
the program was able to correctly access the private vector<game_piece> vectorPiece.



Cases: 


(Successful game)

Command line: Command:  H:\My Documents\Visual Studio 2012\Projects\lab2\Debug>lab2 NineAlmondsGame

Output:

4

3   A A A

2   A A A

1   A A A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 2,2
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 2,0
coordinates accepted

4

3   A A A

2   A   A

1   A   A

0     A

X 0 1 2 3 4


2,2 to 2,0

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 4,2
coordinates accepted

4

3   A A A

2   A   A A

1   A

0

X 0 1 2 3 4


2,2 to 2,0 to 4,2

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 2,4
coordinates accepted

4     A

3   A A

2   A   A

1   A

0

X 0 1 2 3 4


2,2 to 2,0 to 4,2 to 2,4

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 0,2
coordinates accepted

4

3     A

2 A A   A

1   A

0

X 0 1 2 3 4


2,2 to 2,0 to 4,2 to 2,4 to 0,2

Continue this turn? (ie. Yy/Nn) n
4

3     A

2 A A   A

1   A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 1,1
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 1,3
coordinates accepted

4

3   A A

2 A     A

1

0

X 0 1 2 3 4


1,1 to 1,3

Continue this turn? (ie. Yy/Nn) n
4

3   A A

2 A     A

1

0
y
X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 3,2
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 1,4
coordinates accepted

4   A

3   A

2 A

1

0

X 0 1 2 3 4


3,2 to 1,4

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 1,2
coordinates accepted

4

3

2 A A

1

0

X 0 1 2 3 4


3,2 to 1,4 to 1,2

Continue this turn? (ie. Yy/Nn) n
4

3

2 A A

1

0

X 0 1 2 3 4
Enter a valid coordinate (ie 3,4) or 'quit'. 0,2
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 2,2
coordinates accepted

4

3

2     A

1

0

X 0 1 2 3 4


0,2 to 2,2

Continue this turn? (ie. Yy/Nn) n
Huzzah! The game has been completed after 4 turns played.



	This solution is correct because the nine almonds are correctly places in the middle of the game 
board. An updated game board is printed after each move. The program also correctly prints a line showing
the valid moves made so far that turn. The user is continually prompt after each move whether or not she 
wants to continue the turn. When there are no valid moves left, and there is one remaining almond located 
on the central square, the program correctly detects that the game is complete.





(Induce a stalemate)

Command line: Command:  H:\My Documents\Visual Studio 2012\Projects\lab2\Debug>lab2 NineAlmondsGame

Output:

4

3   A A A

2   A A A

1   A A A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 2,2
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 2,4
coordinates accepted

4     A

3   A   A

2   A   A

1   A A A

0

X 0 1 2 3 4


2,2 to 2,4

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 0,2
coordinates accepted

4

3       A

2 A A   A

1   A A A

0

X 0 1 2 3 4


2,2 to 2,4 to 0,2

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 2,0
coordinates accepted

4

3       A

2   A   A

1     A A

0     A

X 0 1 2 3 4


2,2 to 2,4 to 0,2 to 2,0

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 4,2
coordinates accepted

4

3       A

2   A   A A

1     A

0

X 0 1 2 3 4


2,2 to 2,4 to 0,2 to 2,0 to 4,2

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 2,2
coordinates accepted

4

3       A

2   A A

1     A

0

X 0 1 2 3 4


2,2 to 2,4 to 0,2 to 2,0 to 4,2 to 2,2

Continue this turn? (ie. Yy/Nn) n
4

3       A

2   A A

1     A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 3,3
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 1,1
coordinates accepted

4

3

2   A

1   A A

0

X 0 1 2 3 4


3,3 to 1,1

Continue this turn? (ie. Yy/Nn) n

4

3

2   A

1   A A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 1,2
coordinates accepted
Enter a valid coordinate (ie 3,4) or 'quit'. 1,0
coordinates accepted

4

3

2

1     A

0   A

X 0 1 2 3 4


1,2 to 1,0

Continue this turn? (ie. Yy/Nn) y
Enter a valid coordinate (ie 3,4) or 'quit'. 3,2
coordinates accepted

4

3

2       A

1

0

X 0 1 2 3 4


1,2 to 1,0 to 3,2

Continue this turn? (ie. Yy/Nn) n
There is a stalemate after 3 turns played. Better luck next time, chap.


	THis output is correct. The nine almonds are correctly places at the middle of the game board. An updated game 
board is printed after each move. The program also correctly prints a line showing the valid moves made so far that 
turn. The user is continually prompt after each move whether or not she wants to continue the turn. When there are 
no valid moves left, and the one remaining almond does notoccupy the middle square, the program correctly detects 
that there is a stalemate.


(Induce Errors)

The following sequence of inputs will induce possible errors from the program.


4

3   A A A

2   A A A

1   A A A

0

X 0 1 2 3 4

Enter a valid coordinate (ie 3,4) or 'quit'. 3,4
coordinates accepted


Enter a valid coordinate (ie 3,4) or 'quit'. 6,0
ERROR: x-coordinate outside board dimensions
Error: Starting square does not have almond.
Start over with starting coordinate

Enter a valid coordinate (ie 3,4) or 'quit'. 3,4
coordinates accepted


Enter a valid coordinate (ie 3,4) or 'quit'. 3,3
coordinates accepted

Error: Starting square does not have almond.
Start over with starting coordinate

Enter a valid coordinate (ie 3,4) or 'quit'. 3,4
coordinates accepted


Enter a valid coordinate (ie 3,4) or 'quit'. 6,0
ERROR: x-coordinate outside board dimensions
Error: Starting square does not have almond.
Start over with starting coordinate

Enter a valid coordinate (ie 3,4) or 'quit'. 3,4
coordinates accepted


Enter a valid coordinate (ie 3,4) or 'quit'. 3,2
coordinates accepted

Error: Starting square does not have almond.
Start over with starting coordinate

Enter a valid coordinate (ie 3,4) or 'quit'. 2,1
coordinates accepted


Enter a valid coordinate (ie 3,4) or 'quit'. 2,3
coordinates accepted

Error: Final square needs to be empty. Start over.
Start over with starting coordinate

Enter a valid coordinate (ie 3,4) or 'quit'.


As shown above, the program correctly handles user input errors. Until the user enters a valid set of
coordinates, the program will continually output error messages and the user is prompted to reenter 
another set of coordinates.