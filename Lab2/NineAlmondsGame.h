//Jeanine Burke (jvburke@wustl.edu)
//lab2
//NineAlmondsGame.h

#ifndef NINE_ALMONDS_GAME_H
#define NINE_ALMONDS_GAME_H

#include "stdafx.h"
#include "lab2.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;
using std::string;
using std::vector;
using std::ostream;



struct game_piece{
	string name;
	string display;
};



class NineAlmondsGame{

	friend ostream &operator<<(ostream &os, const NineAlmondsGame &almondGame);

public:

	NineAlmondsGame();

	bool done();

	bool stalemate();

	int prompt(unsigned int &input_x, unsigned int &input_y);

	int NineAlmondsGame::valid(unsigned int &startIndex, unsigned int &finishIndex);

	int turn();

	int play();

private: 

	vector<game_piece> vectorPiece; 
};

ostream &operator<<(ostream &os, const NineAlmondsGame &almondGame);



enum game_data{

	LENGTH = 5,
	WIDTH = 5,
	DIMENSION = LENGTH*WIDTH, //25
	MIDDLE_PIECE_INDEX = (DIMENSION-1)/2 //12

};

enum user_data{

	SUCCESS_GAME = 0,
	USER_QUIT = 1,
	INVALID_USER_INPUT=2,
	INVALID_X_COOR = 3,
	INVALID_Y_COOR = 4,
	USER_QUIT_DURING_TURN = 5,
	INVALID_MOVE = 6,
	NO_VALID_INPUTS = 7,
	PLAY_USER_QUIT = 8,
	STALEMATE = 9,

	VALID_MOVE=0,
	NO_START_ALMOND = 1,
	NOT_TWO_SQUARES = 2,
	NO_MIDDLE_ALMOND = 3,
	NO_VALID_MOVES = 4,
	OCCUPIED_FINISH = 5


};


//convert from index to coordinates;
void convertIndextoC(unsigned int & x, unsigned int & y,  unsigned int index);

unsigned int convertCoordtoI(unsigned int x, unsigned int y);

#endif