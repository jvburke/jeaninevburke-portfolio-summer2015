//Jeanine Burke (jvburke@wustl.edu)
//lab2
//NineAlmondsGame.cpp

#include "stdafx.h"
#include "lab2.h"
#include "NineAlmondsGame.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;
using std::string;
using std::ostream;


void convertIndextoC(unsigned int & x, unsigned int & y, unsigned int index){

	x=index%WIDTH;

	y=index/WIDTH;

}

unsigned int convertCoordtoI(unsigned x, unsigned y){

	int res = WIDTH*y+x;
	return res;

}

//constructor
NineAlmondsGame::NineAlmondsGame(){

	game_piece newPiece;
	unsigned int x;
	unsigned int y;
	unsigned int middleX;
	unsigned int middleY;

	//find the index of the middle piece in the vector
	convertIndextoC(middleX, middleY, MIDDLE_PIECE_INDEX);

	//set up the board so that the nine central squares have game pieces
	//and the rest are empty
	for(int i = 0; i<DIMENSION; i++){

		convertIndextoC(x, y, i);

		if(x > (middleX+1) || x < (middleX-1) || y > (middleY+1) || y < (middleY-1)){

			newPiece.name=" ";
			newPiece.display=" ";
		}

		else{

			newPiece.name="brown almond";
			newPiece.display="A";
		}

		vectorPiece.push_back(newPiece);

	}



}

ostream &operator<<(ostream &os, const NineAlmondsGame &almondGame){

	//output the updated board
	for(int i=LENGTH-1; i>=0; i--){
		string boardLine="";
		os<<(i);

		for(int j=WIDTH-1; j>=0; j--){

			unsigned int index = LENGTH*i+j;			

			boardLine=" " + almondGame.vectorPiece[index].display + boardLine;
			//os<<almondGame.vectorPiece[index].display;

		}
		os<<boardLine<<"\n"<<endl;
	}

	os<<"X 0 1 2 3 4"<<endl;
	return os;
}


bool NineAlmondsGame::done(){


	bool finished = true;
	for(size_t i=0; i<DIMENSION; i++){

		//check the status of the middle square
		if(i==MIDDLE_PIECE_INDEX){

			if(vectorPiece[i].display ==" "){
				return false;
			}
		}
		else{
			//check that status of all square other than the middle square
			if(vectorPiece[i].display != " "){
				return false;
			}
		}

	}


	return finished;
}


int NineAlmondsGame::valid(unsigned int &startIndex, unsigned int &finishIndex){

	bool isValid = false;
	//get index of the square being jumped over
	int skippedSquareIndex=(startIndex+finishIndex)/2;
	
	//check if move starts with square that contains almond
	if(vectorPiece[startIndex].name != " "){

		//check that move ends with empty square that is
		//two positions away from original square

		unsigned int xStart;
		unsigned int yStart; //start
		unsigned int xFinish;
		unsigned int yFinish; //finish

		convertIndextoC(xStart, yStart, startIndex);
		convertIndextoC(xFinish, yFinish, finishIndex);

		//test for all eight possible moves
		if(xFinish==(xStart-2) || xFinish==(xStart+2)|| (xFinish == xStart)){

			if(yFinish==(yStart-2) || yFinish==(yStart+2)|| (yFinish==yStart)){

				if(vectorPiece[finishIndex].name == " "){

					//check that there is almond in the square being jumped over

					if(vectorPiece[skippedSquareIndex].name != " "){

						return VALID_MOVE;

					}
					else{
						return NO_MIDDLE_ALMOND;
					}

				}
				else{

					return OCCUPIED_FINISH;
				}

			}
			else{

				return NOT_TWO_SQUARES;
			}

		}
		else{

			return NOT_TWO_SQUARES;
		}


	}

	return NO_START_ALMOND;

}

bool NineAlmondsGame::stalemate(){

	int validMoves=0;

	//iterate throuwgh entire board(vector)and check if there are any valid moves left

	for(unsigned int j = 0; j<DIMENSION; ++j){

		for(unsigned int k = 0; k<DIMENSION; ++k){

			if(valid(j,k) ==  VALID_MOVE){

				validMoves++;
			}

		}

	}

	if(validMoves == 0){

		return true;
	}
	else{
		//there are no valid moes left --> stalemate
		return false;
	}


}

int NineAlmondsGame::prompt(unsigned int &input_x, unsigned int &input_y){

	string input;

	while(true){

		cout<<"\n";
		cout<< "Enter a valid coordinate (ie 3,4) or 'quit'. ";

		//use getline to put user input into string "input"
		getline(cin, input);

		for(size_t k=0; k<input.length(); ++k){
			
			//replace comma in input coordinate with a space
			if(input[k] == ','){

				input[k] = ' ';
			}

		}

		if(input == "quit"){

			return USER_QUIT;
		}

		//try to extract two integers from the input string
		if(stringstream(input)>>input_x>>input_y){

			if(input_x < WIDTH && input_x >=0){

				if(input_y < LENGTH && input_y >= 0){

					cout<<"coordinates accepted"<<endl;
					cout<<"\n";
					return SUCCESS_GAME;
				}
				else{
					cout<<"ERROR: y-coordinate outside board dimensions"<<endl;
					return INVALID_Y_COOR;
				}

			}
			else{
				cout<<"ERROR: x-coordinate outside board dimensions"<<endl;
				return INVALID_X_COOR;
			}


		}
		else{
			cout<<"Could not parse string"<<endl;
			return INVALID_USER_INPUT;
		}
	}
}


int NineAlmondsGame::turn(){

	unsigned int startX;
	unsigned int startY;
	unsigned int finishX;
	unsigned int finishY;

	ostringstream ostrstr;

	bool continueTurn = false;

	//track the number of valid inputs given
	//start a turn at an initial state of 0 valid inputs
	int validInput=0;

	//user will initially enter the while loop when validInput=0
	//once game is underway, the continueTurn condition will allow 
	//the user to continually reenter the while loop
	while(validInput==0 || continueTurn==true){

		//if not a continuing turn, start with new value
		if(continueTurn==false){

			int prompt1 = prompt(startX, startY);

			if(prompt1==USER_QUIT){

				return USER_QUIT_DURING_TURN;
			}
		}


		int prompt2 = prompt(finishX, finishY);

		if(prompt2 == USER_QUIT){

			return USER_QUIT_DURING_TURN;
		}

		else {


			//check if the move is valid
			unsigned int startIndex=convertCoordtoI(startX, startY);
			unsigned int finishIndex=convertCoordtoI(finishX, finishY);

			int res = valid(startIndex, finishIndex);


			if(res == VALID_MOVE){



				validInput++;


				//determine square that is jumped over
				unsigned int skippedX=(startX+finishX)/2;
				unsigned int skippedY=(startY+finishY)/2;


				int skippedIndex = convertCoordtoI(skippedX, skippedY);



				//move the first piece
				//starting square becomes empty
				vectorPiece[startIndex].name=" ";
				vectorPiece[startIndex].display=" ";

				//ending square has almond
				vectorPiece[finishIndex].name="brown almond";
				vectorPiece[finishIndex].display="A";

				//remove almond from square that was jumped over
				vectorPiece[skippedIndex].name=" ";
				vectorPiece[skippedIndex].display=" ";


				//print updated game
				cout<<"\n";

				//called *this will give an updated game board
				cout << *this << endl;

				cout << "\n";

				//if the user is in the beginning of a turn
				if(!continueTurn){

					ostrstr << startX << "," << startY;

				}

				ostrstr << " to " << finishX << "," << finishY;

				//reset ostringstream
				cout << ostrstr.str() <<endl;


				//ask the user if she wants to continue
				string input;
				cout<<endl;
				bool choiceToContinue = false;

				while(!choiceToContinue){

					cout<<"Continue this turn? (ie. Yy/Nn) ";
					getline(cin, input);

					if(input=="Y" || input=="y"){


						continueTurn=true;

						//we are continuing the past turn
						//update the new starting square
						startX=finishX;
						startY=finishY;

						choiceToContinue=true;
					}

					else if(input=="N" || input=="n"){
						cout<<"\n";
						continueTurn=false;
						choiceToContinue=true;
						//	return SUCCESS_GAME;

					}


				}


			}
			else{
				//print out error messages fromo invalid user inputs
				if(res == NO_START_ALMOND){

					cout<<"Error: Starting square does not have almond."<<endl;
				}
				else if(res == NOT_TWO_SQUARES){

					cout<<"Error: Final square must be two squares from starting square."<<endl;
				}
				else if(res == OCCUPIED_FINISH){

					cout<<"Error: Final square needs to be empty. Start over."<<endl;
				}

				if(continueTurn){
					cout<<"Try again."<<endl;
				}
				else{
					cout<<"Start over with starting coordinate"<<endl;
				}

			}
		}
	}


}

int NineAlmondsGame::play(){

	int numTurns=0;

	while(true){

		//initial print of gameboard before user starts turn
		cout<<*this;

		if(turn()==USER_QUIT_DURING_TURN){

			cout<< "The user has quit after " << numTurns << " turns played."<<endl;
			return PLAY_USER_QUIT;
		}
		else{
			numTurns++;

			if(done()==true){

				cout<< "Huzzah! The game has been completed after " << numTurns << " turns played."<<endl;
				return SUCCESS_GAME;
			}
			else{

				if(stalemate()==true){

					cout<< "There is a stalemate after " << numTurns << " turns played. Better luck next time, chap." <<endl;
					return STALEMATE;
				}

			}
		}

	}

}
