//Jeanine Burke (jvburke@wustl.edu)
//Lab2
//lab2.h

#ifndef LAB2_H
#define LAB2_H
#include <string>
using std::string;


enum readArgument{
	PROGRAM_NAME =0,	//0
	INPUT_FILE = 1,		//1
	EXPECTED_ARGUMENTS = 2		//2
};

enum Result{
	SUCCESS = 0,
	ERROR_OPENING_FILE = 1,
	ERROR_TOO_FEW_ARGUMENTS = 2,
	ERROR_INCORRECT_DIMENSIONS = 3,
	ERROR_READING_ALL_FILE_LINES = 4,
	ERROR_VECTOR_SIZE = 5,
	ERROR_ARGUMENT_VALUE = 6

};


int usageFunction(string progName, int message);

#endif