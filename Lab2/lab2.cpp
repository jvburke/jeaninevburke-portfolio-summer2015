//Jeanine Burke (jvburke@wustl.edu)
//Lab2
//

#include "stdafx.h"
#include "lab2.h"
#include "NineAlmondsGame.h"
#include <iostream>
#include <string>

using namespace std;
using std::string;


int usageFunction(string progName, int message){

	//return non-zero value with usage message
	cout<< "usage: " << progName << " NineAlmonds" << endl;
	if(message == ERROR_TOO_FEW_ARGUMENTS){
		return ERROR_TOO_FEW_ARGUMENTS;
	}
	else{
		return ERROR_ARGUMENT_VALUE;
	}
}

int main(int argc,char* argv[])
{
	string a = argv[INPUT_FILE];

	//check that one arguments has been passed to the program
	//and if so, that the passed argument is "NineAlmonds"
	if(argc != EXPECTED_ARGUMENTS){
		return usageFunction(argv[PROGRAM_NAME], ERROR_TOO_FEW_ARGUMENTS);
	}
	else if(a != "NineAlmonds"){
		return usageFunction(argv[PROGRAM_NAME], ERROR_ARGUMENT_VALUE);
	}
	
	//play the game

	NineAlmondsGame game;

	int gamePlay = game.play();

	return gamePlay;
	
}

