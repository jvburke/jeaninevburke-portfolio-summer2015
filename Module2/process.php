<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title> Handling Registration Info </title>
	</head>
	<body>
		<?php
			//echo "starts";
			
			if(empty($_POST['username']) || empty($_POST['password']) ||empty($_POST['password2'])) {
				echo "<h4>Invalid user info entered, please try again</h4>";
				echo "<form action='register.html'>
						<br/>
						<input type='submit' value='Go Back to Registration Page'/>
						</form>";
			}
			else{
				
				$username= (string) $_POST['username'];
				$password= (string) $_POST['password'];
				$password2= (string) $_POST['password2'];
				$securityQ= (string) $_POST['question'];
				$securityA= (string) $_POST['securityAnswer'];

				//start session which may be needed later
				//start it now because it must go before headers
				//session_start();
				
				if($password == $password2){
					echo "passwords match";

					$salt = '$2a$07$usesomadasdsadsadsadasdasdasdsadesillystringfors';
					$password_hash = crypt($password, $salt);	
					$connect = mysql_connect("localhost", "rockyli", "ll");	
					
					if(!$connect){
						
						die('oops connection problem! -->'.mysql_error());
					}
										
					$selectdb = mysql_select_db("module3");
					
					if(!$selectdb){
					
						die('oops database selection problem! -->'.mysql_error());
					}

					$sql_username= mysql_real_escape_string($username);
					$sql_password= mysql_real_escape_string($password_hash);
					$sql_securityQ= mysql_real_escape_string($securityQ);
					$sql_securityA= mysql_real_escape_string($securityA);

					$sql = "INSERT INTO user_mgmt (user_id, user_pwd, security_q, security_a)
							VALUES ('$sql_username', '$sql_password', '$sql_securityQ', '$sql_securityA')";
					
					if(!mysql_query($sql)){
					
						echo("error while registering you...");
						header("Location: register.html");
					
					}
					else{
						echo('You have been successfully registered!');
						echo (
							"<form action='index_login.php'>
								<br/>
								<input type='submit' value='Go Back to Login'/>
							</form>"
							);
					}	
				}		
				else{ 
					echo "<h4>Your passwords do not match</h4>";
					echo "<form action='register.html'>
						<br/>
						<input type='submit' value='Go Back to Registration Page'/>
						</form>";
				}
			}
			
		?>
	</body>
</html>